var quiz = {
    "nombre": "SPH Name Quiz",
    "description": "How many sph names do you know?",
    "pregunta": "¿Cuál es el nombre real de ",
    "preguntas": [
        {"pregunta": "Superman?", "respuesta": "Clark Kent"},
        {"pregunta": "Spiderman?", "respuesta": "Peter Parker"},
        {"pregunta": "Batman?", "respuesta": "Bruce Wayne"},
        {"pregunta": "Wonderwoman?", "respuesta": "Diana Prince"},
    ],
}

var score = 0;

play(quiz);

function play(quiz){
    for(var i=0, pregunta, respuesta, max=quiz.preguntas.length; i<max; i++){
        pregunta= quiz.preguntas[i].pregunta;
        respuesta= preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();


    function preguntar(pregunta){
        return prompt(quiz.pregunta + pregunta);
    }

    function verificar(respuesta){  
        if(respuesta === quiz.preguntas[i].respuesta){
            alert("Correcto.");
            score += 10;
        } else{
            alert("Incorrecto.");
            score -= 5;
        }
    }

    function gameOver(){
        alert("Game over. Su puntuacion fue " + score + " puntos.");

    }
}

