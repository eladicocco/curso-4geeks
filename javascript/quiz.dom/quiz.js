var quiz = {
    "nombre": "SPH Name Quiz",
    "description": "How many sph names do you know?",
    "pregunta": "¿Cuál es el nombre real de ",
    "preguntas": [
        {"pregunta": "Superman?", "respuesta": "Clark Kent"},
        {"pregunta": "Spiderman?", "respuesta": "Peter Parker"},
        {"pregunta": "Batman?", "respuesta": "Bruce Wayne"},
        {"pregunta": "Wonderwoman?", "respuesta": "Diana Prince"},
    ],
}

var score = 0;
var $pregunta = document.getElementById("pregunta");
var $score = document.getElementById("score");
var $feedback = document.getElementById("feedback");

/*
function update(elemento, contenido, estilo){
    var p = elemento.firstChild || document.createElement("p");
    p.textContent = contenido;
    elemento.appendChild(p);
    if (estilo){
        p.className = estilo;
    }
}*/

function update(elemento, contenido, estilo){
    var p = elemento.firstChild || document.createElement("p");
    p.textContent = contenido;
    elemento.appendChild(p);
    if (estilo){
        p.className = estilo;
    }
}

play(quiz);

function play(quiz){
    for(var i=0, pregunta, respuesta, max=quiz.preguntas.length; i<max; i++){
        pregunta= quiz.preguntas[i].pregunta;
        respuesta= preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();


    function preguntar(pregunta){
        update($pregunta, quiz.pregunta + pregunta);
        return prompt("Answer here");
    }

    function verificar(respuesta){  
        if(respuesta === quiz.preguntas[i].respuesta){
            //alert("Correcto.");
            update($feedback, "Correct.", "cor");
            score += 10;
            update($score, score);
        } else{
           // alert("Incorrecto.");
           update($feedback, "Incorrect.", "inc");
            score -= 5;
            update($score, score);
        }
    }

    function gameOver(){
        //alert("Game over. Su puntuacion fue " + score + " puntos.");
        update($pregunta, "Game Over. You scored " + score + " points.")
    }
}

