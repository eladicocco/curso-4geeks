var quiz= [
    ["¿Cuál es el nombre real de Superman?","Clark Kent"],
    ["¿Cuál es el nombre real de Spiderman?","Peter Parker"],
    ["¿Cuál es el nombre real de Batman?","Bruce Wayne"],
    ["¿Cuál es el nombre real de Wonderwoman?","Diana Prince"],
];

var score = 0;
for(var i=0, max=quiz.length; i<max; i++){
    var respuesta= prompt(quiz[i][0]);
    if(respuesta === quiz[i][1]){
        alert("Correcto.");
        score += 10;
    } else{
        alert("Incorrecto.");
        score -= 5;
    }
}

alert("Game over. Su puntuacion fue " + score + " puntos.");