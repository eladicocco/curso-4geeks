var parrafo = document.getElementsByTagName("p");
console.log(parrafo);

parrafo[0].innerHTML = "Hola desde JS";

console.log(document.body);
//document.images
//document.links
//document.forms

forms = document.forms;
for(var i=0; i<forms.length; i++){
    console.log(forms[i].id);

    form1= forms[i];
    form1.append("<p>Hola " + i +"</p>");
}

form = document.getElementById("id1");
form.style.background = "red";

lista = document.getElementById("lista"); //1 element
lista2 = document.getElementsByName("lista2"); //collection
listas = document.getElementsByClassName("lista"); //collection 
listas2 = document.getElementsByTagName("ul"); //collection

body = document.querySelector("body"); //toma notacion css
console.log(body);

lista = document.querySelector("#lista"); //toma notacion css
console.log(lista.innerHTML="<li>UML</li>");

p = document.querySelector("p");
console.log(p.firstChild);

lista = document.querySelector("ul");
console.log(lista);
console.log(lista.getAttribute("id")); //getAttribute
//console.log(lista.setAttribute("class", "naranja")); !!!!!!!!!!setAttribute 
//var parrafo = document.createElement("p");//createElement("_")
//var texto = document.createTextNode("agregar texto");
//parrafo.appendChild(texto);
//document.body.append(parrafo); //append siempre agrega al final del doc
//document.body.insertBefore(); lugar especifico 

p1 = document.createElement("p");
txtP1 = document.createTextNode("Texto Parrafo 1");
p1.appendChild(txtP1);
document.body.appendChild(p1);

p2 = document.createElement("p");
txtP2 = document.createTextNode("Texto Parrafo 2");
p2.appendChild(txtP2);
document.body.insertBefore(p2, p1);

p = document.getElementsByTagName("p");
textViejo = p[0].firstChild;
console.log(textViejo);
nuevoTexto = document.createTextNode("Spm");
console.log(nuevoTexto);
p[0].replaceChild(nuevoTexto, textViejo);
