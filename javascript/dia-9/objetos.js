//objetos en js
//+ amplio, allows properties
var superman = { 
    nombre:"Superman",
    "nombre real": "Clark Kent", 
    volar: function(){},
    laser: function(){},
} 
//var superman = new object {}

console.log(superman);
console.log(superman.nombre);
console.log(superman.volar());
console.log(superman["volar"]());

//conocer si el objeto tiene cierta propiedad
console.log("edad" in superman);
console.log(superman.edad !==undefined)

//saber todos los atributos/propiedades del objeto
for(var key in superman){
    console.log(key + ":" + superman[key]);
}

//add propiedad 
superman.ciudad="Metropolis";
console.log(superman);

//objeto anidado o conjunto de objeto
justice_league={
    superman: {realName: "Clark Kent."},
    batman: {realName: "Bruce Wayne."},
    wonderwoman: {realName: "Diana Prince."},

}

console.log(justice_league.superman.realName);

function mensaje(options){
    options = options || {};
    saludo = options.saludo || "Hola";
    nombre = options.nombre || "Visitante";
    edad = options.edad || 18;

    return saludo + "! Mi nombre es " + nombre + " y tengo " + edad + " años de edad.";
}
datos={nombre: "JHS", edad: 24}
console.log(mensaje(datos));
console.log(mensaje({nombre:"MYG", edad:25}));

//JS object notation or JSON
var alumno = '{"nombre": "YN", "edad": 22, "ciudad": "ccs"}';
alumno = JSON.parse(alumno);
console.log(alumno.nombre);

var spm = JSON.stringify(superman);
console.log(spm);

//objetos especiales= Math(const matematicas)
console.log(Math.PI);
console.log(Math.LN2);

var sueldo = 1320.45;
console.log(Math.ceil(sueldo)); //redondea
console.log(Math.floor(sueldo)); //quita decimales
console.log(Math.round(sueldo)); //redondea al mas cercano

console.log(Math.random()); 
console.log(Math.floor(Math.random() * 5) + 1);

console.log(Math.abs(-1345.456)); //val absoluto

console.log(Math.pow(3,2)); //cuadrado

//Date(); fecha y hora actual
hoy = new Date();
console.log(hoy);
console.log(hoy.getMonth()); //getDay, getFullYear

var dias = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"];
var meses = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"];
var fecha = new Date();
console.log(dias[fecha.getDay()]); 
console.log(meses[fecha.getMonth()]);
console.log(fecha.getHours()+ ":" +fecha.getMinutes());

//expresiones regulares
var pattern_1 = /\w+ing/;
var pattern_2 = new RegExp('\w+ing'); 
prueba = pattern_1.test("Clean");
console.log(prueba);
console.log(pattern_1.exec("Joke"));
console.log(pattern_1.exec("Joking"));
var soloVocales= /[aeiou]/;
console.log(soloVocales.test("rome"));

var alf= /[A-Z]/; //rango.
var num= /[0-9]/;
var alfm= /[^A-Z]/; //^ niega la expresion aka ningun carac puede estar en mayus.

// \w = [A-Za-z0-9_] cualquier palabra.
// \W = [^A-Za-z0-9_] cualquier caracter fuera de w, no sea palabra. 
// \d = [0-9] cualquier digito.
// \D = [^0-9] cualquier no digito.
// \s = [\t\r\n\f] espacio en blanco.
// \S = [^\t\r\n\f] espacios no en blanco.

//Propiedades de las expresiones regulares
// /[a-z]/g, 'global', devuelve todas las coincidencias de la busqueda.
// /[a-z]/i, 'ignore case' ignora si es minus o mayus.
// /[a-z]/m, 'multline' busca en todo el texto.
