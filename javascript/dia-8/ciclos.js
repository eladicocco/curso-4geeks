//Ciclos: while
var n=1
while(n<=10){
    console.log(n);
    n++; //n=n+1
}; //ejecuta mientras se cumpla la condicion inicial

//do while, ejecuta antes de evaluar la condicion, siempre imprime aunque sea una vez
n=1;
do {
    console.log(n);
    n++;
}while(n<=10);

//For. Parametros: inicializador; condicion /ejecuta el cod/; incremental/despues). 
for(var i=0; i<=10; i++){
    console.log(i);
};

var multi= [[1, 1], [10, 10], [20, 20]];
for (var i=0; i<multi.length; i++){
    for (var j=0; j<multi[i].length; j++){
        console.log(multi[i][j]);
    }
};




