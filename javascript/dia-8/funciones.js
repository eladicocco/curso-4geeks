//definir funciones
function saludo(){
    console.log("Invocando una funcion");
};
saludo();

//funcion por expresion(?)
var saludo2= function(){
    console.log("Invocando una funcion (saludo2)");
}
saludo2();
function mayor(){
    var edad = 18;
    if(edad<18){
        return "Es menor";
    }else{
        return "Es mayor";
    }
}
console.log(mayor());

//-------------------------------------------------------------------------

function mayor2(){
    var edad = 18;
    if(edad<18){
        return false;
    }else{
        return true;
    }
}

if (mayor2()){
    console.log("Es Mayor");
}else{
    console.log("Es menor");
}

function sumar(a, b){
    return a+b;
}
console.log(sumar(40, 23));

var counter=0
function contar(){
    return counter+=1;
}
console.log(contar());

//uso del ForEach nota= val de arr, index pos

var arr=[12, 34, 45, 54];
arr.forEach(function(nota, index){
    console.log("Nota: " + index + "-" + nota);
});

function square(nota){
    return nota * nota;
}
arr.map(function(nota, index){
    console.log(nota*nota);
});

total= arr.reduce(function(anterior, siguiente){
    return anterior + siguiente;
})
console.log(total);

//filter
numeros = arr.filter(function(numero){
    return numero%2===0;
});
console.log(numeros);

var nombres = ["ai", "ei", "oi", "ui"];
nombre = nombres.filter(function(nombre){
    if(nombre.toLocaleLowerCase().indexOf("i")){
        return nombre;
    }
});
console.log(nombre);