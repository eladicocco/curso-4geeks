//sentenicia If, if else

var a=5;
var b=3;
var c=8;

//if(a==b){
    //sentencias a ejecutar
//}

if(a>b){
    console.log("a es mayor que b")
}else{
    console.log("b es mayor que a")
}

if(a>b && a>c){
    console.log("a es el mayor")
}else if(b>a && b>c){ 
    console.log("b es el mayor")
}else {
    console.log("c es el mayor")
};

var edad=13
if(edad<18){
    console.log("no puede ver este contenido")
}

//Operador ternario
n=5;
n%2===0 ? console.log("Es par") : console.log("Es impar");

flag=true;

flag ? alert("verdadero") : alert("falso");

edad=21
edad>18 ? console.log("es mayor") : console.log("es menor");

//Switch case
var opt = 4;
 //if
 if (opt===1){
     console.log("Opcion 1")
 }else if (opt===2){
     console.log("Opcion 2")
 }else if (opt===3){
    console.log("Opcion 3")
 } else if (opt===4){
    console.log("Opcion 4")
 }else if (opt===5){
    console.log("Opcion 5")
 }else if (opt===6){
    console.log("Opcion 6")
 }else {
     console.log("incorrecta")
 }

 //enswitchcase
 switch(opt){
     case 1:
        console.log("Opcion 1");
        break;
     case 2:
        console.log("Opcion 2");
        break;
     case 3:
        console.log("Opcion 3");
        break;
     case 4:
        console.log("Opcion 4");
        break;
     case 5:
        console.log("Opcion 5");
        break;
     case 6:
        console.log("Opcion 6");
        break;
     default:
        console.log("Incorrecta");
        break;
 }