//comentarios cortos

/*
comentario largo o multiples lineas
*/

// js grammar
a="dkdfj" 
alert(a)
// or
a = "jdij";
alert(a);

//code blocks, are defined by {}, usually used w function
{
    //cont 2 sent
    var a= "hihdi";
    alert(a);
}

//kinds of data
/*
string letters
number numbers
boolean t/f
undefined 
null 
object arreglos, nulls
*/
let nombre="yyyy"; typeof nombre; //string
let edad = "5"; typeof edad; //number
let soltero=true; typeof soltero; //boolean
typeof apellido; //undefined
let flag = null; typeof flag; // object 
let estudiante = {nombre: 'eknife', apellido:'ifni'}; typeof estudiante; // object

//escape caracteres, colocar \ antes del caracter que se quiere interpretar \n linea nueva
let apellido ="D\'Ambrosio";
let mensaje = "cita:\"esto es una cita\""; //\* escapando comillas dobles
let mensaje2= "primera linea \n segunda";
let mensaje3= "prueba \r"; //\r {enter} retorno de carga
let mensaje4= "separado por \t tab " //\t tabulado 

//rules to define variables (they're case sensitive)
$nombre ="mdson"
_nombre ="mdson"
nombre ="mdson"
primer_nombre ="mdson"
primerNombre ="mdson"
nombre1="mdson" //can't start w a number but can contain it

/*Palabras reservadas
abstract, boolean, break, byte, case, catch, char, class, const, continue, debugger,
default, delete, do, double, else, enum, export, extends, false, final, finally, float, for,
function, goto, if, implements, import, in instanceof, int, interface, long, native, new, null,
package, private, protected, public, return, short, static, super, switch, synchronized, this, 
throw, throws, transient, true, try, typeof, var, volatile, void, while, with 
*/

// Asignations
var a ="";
var a, b, c;
var a=1, b, c=3;
b=7;




