window.onload = function(){ // Acciones tras cargar la pagina
    pantalla = document.getElementById("textoPantalla");
    document.onkeydown = teclado;
}

x = "0" // guardar numero en pantalla
xi =  1 // iniciar numero en pantalla 1=si, 0=no
coma = 0 // estado coma decimal 0=no 1=si

ni = 0; // numero oculto o en espera
op = "no" // operacion en curso; no = sin operacion;

var $num1 = document.getElementById("num1");
var $num2 = document.getElementById("num2");
var $num3 = document.getElementById("num3");
var $num4 = document.getElementById("num4");
var $num5 = document.getElementById("num5");
var $num6 = document.getElementById("num6");
var $num7 = document.getElementById("num7");
var $num8 = document.getElementById("num8");
var $num9 = document.getElementById("num9");
var $num0 = document.getElementById("num0");

var $sumar = document.getElementById("sum");
var $restar = document.getElementById("rest");
var $multi = document.getElementById("multi");
var $dividir = document.getElementById("dividir");

var $igual = document.getElementById("igual");

var $retro = document.getElementById("retro");

$num1.addEventListener("click", function(){ numero('1'); });
$num2.addEventListener("click", function(){ numero('2'); });
$num3.addEventListener("click", function(){ numero('3'); });
$num4.addEventListener("click", function(){ numero('4'); });
$num5.addEventListener("click", function(){ numero('5'); });
$num6.addEventListener("click", function(){ numero('6'); });
$num7.addEventListener("click", function(){ numero('7'); });
$num8.addEventListener("click", function(){ numero('8'); });
$num9.addEventListener("click", function(){ numero('9'); });
$num0.addEventListener("click", function(){ numero('0'); });

$sumar.addEventListener("click", function(){ operar('+') });
$restar.addEventListener("click", function(){ operar('-') });
$multi.addEventListener("click", function(){ operar('*') });
$dividir.addEventListener("click", function(){ operar('/') });

$igual.addEventListener("click", function(){ igualar() });

$retro.addEventListener("click", retro);

function numero(xx){ // recoge el numero pulsado 
    if(x == "0" || xi == 1) { // inicializar 
        pantalla.innerHTML = xx; // mostramos en pantalla
        x = xx; // guardo el numero
        if(xx == ".") {
            pantalla.innerHTML = "0."; // escribimos 0.
            x = xx; // guardo el numero
            coma = 1; // cambiar estado de la coma
        }
    } else { // continuar escribiendo un numero
        if(xx == "." && coma == 0){ // si escribimos una coma decimal por primera vez
            pantalla.innerHTML += xx;
            x += xx;
            coma = 1; // cambiar el estado de la coma
        } // si intentamos escribir una segunda coma decimal no realizar ninguna accion
        else if(xx=="." && coma == 1) {}
        else {
            pantalla.innerHTML += xx;
            x += xx;
        }
    }
    xi = 0 // el numero esta iniciado y podemos ampliarlo
}

function operar(s){
    ni = x; // colocamos el 1° numero en "numero en espera" para poder escribir el 2°
    op = s; // guardar el tipo de operacion que voy a realizar
    xi = 1; // inicializar pantalla
}

function igualar(){
    if(op=="no"){
        pantalla.innerHTML = x;
    } else {
        sl = ni + op + x; // escribimos la operacion en una cadena
        sol = eval(sl); // convertimos la cadena a codigo y resolvemos
        pantalla.innerHTML = sol; // escribimos el resultado en pantalla
        x = sol; // guardamos la solucion para realizar otra operacion
        op = "no"; // ya no tenemos operaciones pendientes
        xi = 1; // se puede reiniciar la pantalla
    }
}

function retro(){
    cifras = x.length; // hayar numero de caracteres en pantalla
    uc = x.substr(cifras - 1, cifras); // informacion del ultimo caracter
    x = x.substr(0, cifras - 1); // quitamos el ultimo caracter

    if(x == ""){
        x = "0";
    } // si ya no quedan caracteres colocamos el 0

    if(uc == "."){
        coma = 0;
    } // si hemos quitado la coma, se permite escribirla de nuevo

    pantalla.innerHTML = x; // mostramos resultado en pantalla
}

function borradoParcial(){
    pantalla.innerHTML = 0; // borrado de pantalla
    x = 0; // Borrado indicador numero en pantalla
    coma = 0; // reiniciamos el uso de la coma
}

function borradoTota(){
    pantalla.innerHTML = 0; // borrado de pantalla
    x = 0; // Borrado indicador numero en pantalla
    coma = 0; // reiniciamos el uso de la coma
    ni = 0; // inicializamos el numero en espera a 0
    op = "no"; // borramos la operacion en curso 
}